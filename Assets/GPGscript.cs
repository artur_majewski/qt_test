﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif
using UnityEngine.UI;


public class GPGscript : MonoBehaviour
{

    public static bool signInAtStart = true;


    public void Start()
    {


#if UNITY_ANDROID


        // Create client configuration
        PlayGamesClientConfiguration config = new
            PlayGamesClientConfiguration.Builder()
            .Build();

        // Enable debugging output (recommended)
        PlayGamesPlatform.DebugLogEnabled = true;

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);
       
        if (!PlayGamesPlatform.Instance.localUser.authenticated && signInAtStart)
        {
            SignIn();
            signInAtStart = false;
            ShowAchievements();
        }
        StartCoroutine(LaLala());
#elif UNITY_IOS
        if (!Social.localUser.authenticated)
            SignIn();
#endif


    }
    IEnumerator LaLala()
    {
        yield return new WaitForSecondsRealtime(5);
        if (!PlayGamesPlatform.Instance.localUser.authenticated && signInAtStart)
        {
            SignIn();
            signInAtStart = false;
            ShowAchievements();
        }
    }

    public void SignInCallback(bool success)
    {
        if (success)
        {
            Debug.Log("BACH (Lollygagger) Signed in!");

            // Change sign-in button text


        }
        else
        {
            Debug.Log("BACH (Lollygagger) Sign-in failed...");

        }
    }


    public void SignIn()
    {

#if UNITY_ANDROID
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {

            // Sign in with Play Game Services, showing the consent dialog
            // by setting the second parameter to isSilent=false.
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
        }
        else
        {

            // Sign out of play games
            PlayGamesPlatform.Instance.SignOut();
        }

#endif
#if UNITY_IOS
        Debug.Log("MainMenuEvents::SignIn (signInButton clicked), platform: iOS");
        if (!Social.localUser.authenticated)
        {

            Debug.Log("MainMenuEvents::SignIn user not authenticated");
            Social.localUser.Authenticate(success =>
            {
                if (success)
                {
                    Debug.Log("Succesfully logged to game center or sth: " + success);
                    GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
                    Debug.Log("getting to log achievements (and synchronize)");
                    LogAchievements(); // IN ORDER TO SYNCHRONIZE
                }

                else
                {
                    //authStatus.text = "Sign in failed";
                    Debug.Log("Failed to authenticate");
                }

            });
        }
        else
        {
            ShowAchievements();
        }



#endif

    }

    public void ShowAchievements()
    {
        Debug.Log("BACH showing achievmenets");
#if UNITY_ANDROID
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowAchievementsUI();
        }
        else
        {
            Debug.Log("BACH Cannot show Achievements, not logged in");
        }

#endif

#if UNITY_IOS
        if (Social.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
        }
#endif
    }


    public void UnlockTestAchievement()
    {
#if UNITY_IOS

        // simply unlock:
        Achievement a = new Achievement("tralaala", "FurryTestAchievement123", true);
        a.Unlock(); // has to be eeargh integer

        // debug:

#endif

    }

    public void IncrementTestAchievement()
    {
#if UNITY_IOS

        Achievement a = new Achievement("tralaala", "FurryTestAchievement123_2", true);
        a.Unlock(2); // increment by 2 ("percent")

        // debug:

#endif

    }
    void LogAchv(string data)
    {
        Debug.Log(data);

        // debugText.text += (data + "\n");

    }


    public void ResetIOSAchievements()
    {
#if UNITY_IOS
        // reset the remote achievements
        GameCenterPlatform.ResetAllAchievements((resetResult) =>
        {
            LogAchv((resetResult) ? "Reset done." : "Reset failed.");
        });


        // reset the local achievements
        foreach (Achievement a in AchievementList.achievements.Values)
        {
            PlayerPrefs.SetFloat("ios_" + a.iOS_ID, 0); // local
        }

#endif
    }
    List<UnityEngine.SocialPlatforms.IAchievement> myAchievementsLaLa;
    /**
<summary>If the current global raspb counter val is SMALLER than the argument, replace it with the argument</summary>
    **/


    public void LogAchievements() // logs and puts the achievements (just the unlocked and/or partially unlocked) into the list (local)
    {
        //       myAchievementsLaLa = new List<UnityEngine.SocialPlatforms.IAchievement>();
#if UNITY_IOS
        LogAchv("log achievements test:" + System.DateTime.Now.ToShortTimeString());
        Social.LoadAchievements(achievements =>
        {
            if (achievements.Length > 0)
            {
                // Logs:
                //LogAchv("Got " + achievements.Length + " achievement instances"); // ok

                ////    myAchievementsLaLa.InsertRange(0, achievements);

                //string myAchievements = "My achievements:\n";
                //foreach (UnityEngine.SocialPlatforms.IAchievement achievement in achievements)
                //{
                //    myAchievements += "\t" +
                //        achievement.id + " " +
                //        achievement.percentCompleted + " " +
                //        achievement.completed + " " +
                //        achievement.lastReportedDate + "\n";
                //}

                //LogAchv(myAchievements);// ok (twice???)
                //WORKS ONLY FOR IOS

                foreach (UnityEngine.SocialPlatforms.IAchievement achievement in achievements) // pobrane
                {
//TODO:implement
                    foreach (AchievementList.AchievementID acId in AchievementList.achievements.Keys)
                    { // lokalne
                        Achievement ac = AchievementList.achievements[acId];
                        if (ac.iOS_ID == achievement.id && ac.currentIOSProgress < achievement.percentCompleted)
                        {// jest taki zdalny jak lokalny i zdalny ma większy progress
                         // uaktualnij lokalny:
                            ac.currentIOSProgress = (float)achievement.percentCompleted;
                            PlayerPrefs.SetFloat("ios_" + ac.iOS_ID, ac.currentIOSProgress); // local... na iOSie to powinno wystarczyć?
                            //Debug.LogError("setting the ios_ " + ac.iOS_ID + " value in prefs to" + ac.currentIOSProgress);


                        }
                    }

                    // logs:
                    //myAchievements += "\t" +
                    //    achievement.id + " " +
                    //    achievement.percentCompleted + " " +
                    //    achievement.completed + " " +
                    //    achievement.lastReportedDate + "\n";
                }


            }
            else
                LogAchv("No achievements returned");
        });

 
#endif
    }
}
